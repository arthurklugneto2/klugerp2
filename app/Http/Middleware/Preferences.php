<?php

namespace App\Http\Middleware;

use Closure;
use View;
use App\Parametros;

class Preferences
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $parameters = $this->getParameters();
        View::share('parametros', $parameters);
        return $next($request);
    }

    public function getParameters(){
        
        $qtdParametros = Parametros::count();
        if( $qtdParametros == 0 ){
            $registro = new Parametros;
            $registro->nomeEmpresa = 'Nome da Empresa';
            $registro->save();
            return $registro;
        }else{
            return Parametros::all()->take(1)[0];
        }       
    }
}
