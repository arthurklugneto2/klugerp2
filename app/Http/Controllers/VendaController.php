<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\VendaService;
use Validator;
use View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class VendaController extends Controller
{
    
    protected $vendaService;

	public function __construct(VendaService $vendaService)
	{
        $this->middleware('auth');
        $this->vendaService = $vendaService;
	}
	
	public function index()
	{
		$registros = $this->vendaService->getAll();
		return View::make('venda.index')
		->with('vendas', $registros);
	}
	
	public function create()
	{
		$clientes = $this->vendaService->getClientes();
        $produtos = $this->vendaService->getProdutos();
        $vendedores = $this->vendaService->getVendedores();
		
		return View::make('venda.create')
		->with('clientes', $clientes)
        ->with('produtos', $produtos)
        ->with('vendedores', $vendedores);
	}
	
	public function store(Request $request)
	{
        $rules = array(
            'dataVenda'       => 'required',
            'cliente_id' => 'required'
        );
        $validator = Validator::make($request->input(), $rules);

        if ($validator->fails()) {
			return Redirect::to('venda/create')
			->withErrors($validator);
		} else {
            $id = $this->vendaService->save($request->input());
		return Redirect::to('venda/'.$id.'/edit');
        }		
	}
	
	public function edit($id)
	{
        $venda = $this->vendaService->findById($id);

        if( $venda->situacao == 'finalizada' ){
            Session::flash('messageErro', 'Venda ja foi finalizada. Não é possível editar.');
            return Redirect::to('venda');
        }

		$produtos = $this->vendaService->getProdutos();
		$vendasProdutos = $this->vendaService->getSaidaProdutos($id);
		
		return View::make('venda.edit')
		->with('produtos', $produtos)
		->with('venda', $venda)
		->with('vendasProdutos', $vendasProdutos);
		
	}
	
	public function addItem(Request $request,$id)
	{
		$rules = array(
            'produtos'       => 'required'
        );
        $validator = Validator::make($request->input(), $rules);

        if (!$validator->fails()){
            $this->vendaService->addItem($request->input(),$id);
        }
		
		$produtos = $this->vendaService->getProdutos();
        $vendasProdutos = $this->vendaService->getSaidaProdutos($id);
        $venda = $this->vendaService->findById($id);
        $produtoVenda = $this->vendaService->getProduto($request->input('produtos'));
				
		return View::make('venda.edit')
		->with('produtos', $produtos)
		->with('venda', $venda)
		->with('vendasProdutos', $vendasProdutos)
		->with('produtoAtual', $produtoVenda);		
	}

	public function removeItem($id)
	{
        $venda = $this->vendaService->findBySaidaProdutoId($id);
        $produtos = $this->vendaService->getProdutos();
        $this->vendaService->removeItem($id);
		
		$vendasProdutos = $this->vendaService->getSaidaProdutos($id);		
		
		return View::make('venda.edit')
		->with('produtos', $produtos)
		->with('venda', $venda)
		->with('vendasProdutos', $vendasProdutos);
	}
	
	public function finalizarVenda($id)
	{
		
		$venda = $this->vendaService->findById($id);
		$vendasProdutos = $this->vendaService->getSaidaProdutos($id);
		
		$pagamentos = $this->vendaService->getSaidaPagamentos($id);
		$formasPagamento = $this->vendaService->getFormaPagamentos();
		$valorTotal = 0;
		
		foreach ($vendasProdutos as $key => $value)
		{
			$valorTotal += $value->quantidade * $value->valor;
		}
		
		$venda->valor = $valorTotal;
		$venda->save();
		
		$dadosArray = array(
				'total' => $valorTotal
		);
		
		return View::make('venda.finalizar')
		->with('venda', $venda)
		->with('produtos', $vendasProdutos)
		->with('dados', $dadosArray)
		->with('formasPagamento', $formasPagamento)
		->with('pagamentos', $pagamentos);
		
	}
	
	public function addPagamento(Request $request,$id)
	{
		if(!empty($request->input('valorPagamento')))
        {
            $this->vendaService->adicionarPagamento($request->input(),$id);            
        }
        
		$pagamentos = $this->vendaService->getSaidaPagamentos($id);
        $formasPagamento = $this->vendaService->getFormaPagamentos();
        $venda = $this->vendaService->findById($id);
		
		return View::make('venda.finalizar')
		->with('venda', $venda)
		->with('formasPagamento', $formasPagamento)
		->with('pagamentos', $pagamentos);
    }
    
    public function removePagamento($id)
    {
        $idConta = $this->vendaService->removePagamento($id);
        $pagamentos = $this->vendaService->getSaidaPagamentos($idConta);
        $formasPagamento = $this->vendaService->getFormaPagamentos();
        $venda = $this->vendaService->findById($idConta);
		
		return View::make('venda.finalizar')
		->with('venda', $venda)
		->with('formasPagamento', $formasPagamento)
		->with('pagamentos', $pagamentos);
    }
	
	public function destroy($id)
	{
		$this->vendaService->remove($id);
	
		Session::flash('message', 'Venda apagada com sucesso!');
		return Redirect::to('venda');
	}
	
}
