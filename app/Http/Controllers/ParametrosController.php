<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use App\Parametros;
use Illuminate\Support\Facades\Redirect;

class ParametrosController extends Controller
{
    public function index(){
        return View::make('parametros.index')
        ->with('parametro', Parametros::all()->take(1)[0]);
    }

    public function update(Request $request){
        $parametro = Parametros::all()->take(1)[0];

        $parametro->nomeEmpresa = $request->input('nomeEmpresa');
        $parametro->corCabecalho = $request->input('corCabecalho');
        $parametro->save();

        return Redirect::to('home');
    }
}
