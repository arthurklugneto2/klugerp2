<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ClienteService;
use Validator;
use View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Parametros;

class ClienteController extends Controller
{
    
    protected $clienteService;

	public function __construct(ClienteService $clienteService)
	{
        $this->middleware('auth');
        $this->clienteService = $clienteService;
	}
	
	public function index()
	{
		$registros = $this->clienteService->getAll();
	
		return View::make('cliente.index')
		->with('clientes', $registros);
	}
	
	public function create()
	{
		return View::make('cliente.create');
	}
	
	public function store(Request $request)
	{
	
		$rules = array(
				'nome' => 'required'
		);
		$validator = Validator::make($request->input(), $rules);
	
		if ($validator->fails()) {
			return Redirect::to('cliente/create')
			->withErrors($validator);
		} else {
	
			$this->clienteService->save($request->input());
	
			Session::flash('message', 'Cliente adicionado com sucesso!');
			return Redirect::to('cliente');
		}
	}
	
	public function show($id)
	{
		$registro = $this->clienteService->findById($id);
	
		return View::make('cliente.show')
		->with('cliente', $registro);
	}
	
	public function edit($id)
	{
		$registro = $this->clienteService->findById($id);
	
		return View::make('cliente.edit')
		->with('cliente', $registro);
	}
	
	public function update(Request $request,$id)
	{
		$rules = array(
				'nome' => 'required'
		);
		$validator = Validator::make($request->input(), $rules);
		
		if ($validator->fails()) {
			return Redirect::to('cliente/create')
			->withErrors($validator);
		} else {
		
			$this->clienteService->update($request->input(),$id);
		
			Session::flash('message', 'Cliente alterado com sucesso!');
			return Redirect::to('cliente');
		}
	}
	
	public function destroy($id)
	{
		$this->clienteService->remove($id);
	
		Session::flash('message', 'Cliente apagado com sucesso!');
		return Redirect::to('cliente');
	}
	
}
