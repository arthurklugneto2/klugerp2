<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use View;
use App\Services\RelatorioService;
use Excel;
use App\Exports\EstoqueExport;
use App\Exports\SaidasExport;
use App\Exports\EntradaExport;

class RelatorioController extends Controller
{
    
    protected $relatorioService;

	public function __construct(RelatorioService $relatorioService)
	{
        $this->middleware('auth');
        $this->relatorioService = $relatorioService;
	}
	
	public function index()
	{
		return View::make('relatorio.index');
	}
	
	public function relatorioSaidas(Request $request)
	{
        $dataInicial = $request->input('dataInicial').' 00:00:00';
        $dataFinal = $request->input('dataFinal'). '23:59:59';
        
        return Excel::download(new SaidasExport($dataInicial,$dataFinal),'vendas.xlsx');		
	}
	
	public function relatorioEntradas(Request $request)
	{
		$dataInicial = $request->input('dataInicial').' 00:00:00';
        $dataFinal = $request->input('dataFinal'). '23:59:59';
        
        return Excel::download(new EntradaExport($dataInicial,$dataFinal),'compras.xlsx');
	}
	
	public function relatorioEstoque()
	{
		return Excel::download(new EstoqueExport(),'estoque.xlsx');
	}
	
}
