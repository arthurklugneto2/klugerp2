<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use View;
use App\Services\DashboardService;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Entrada;
use App\Saida;
use App\Cliente;
use App\Produto;
use DB;
use Carbon\Carbon;

class HomeController extends Controller
{
    protected $dashBoardService;

    public function __construct(DashboardService $dashBoardService)
    {
        $this->middleware('auth');
        $this->dashBoardService = $dashBoardService;
    }

    public function index()
    {
        $produtoMaisVendido = $this->dashBoardService->getMaisVendido();

        $vendasTotal = $this->dashBoardService->getTotalVendas();
        $valorRecebido = $this->dashBoardService->getVendasRecebidas();
        $valorAReceber = $this->dashBoardService->getVendasReceber();

        $comprasTotal = $this->dashBoardService->getTotalCompra();
        $valorPago = $this->dashBoardService->getComprasPagas();
        $valorAPagar = $this->dashBoardService->getComprasPagar();
        
        return View::make('home')
        ->with('produtoMaisVendido', $produtoMaisVendido)

        ->with('vendasTotal', $this->formatMoney($vendasTotal))
        ->with('valorRecebido', $this->formatMoney($valorRecebido))
        ->with('valorAReceber', $this->formatMoney($valorAReceber))

        ->with('comprasTotal', $this->formatMoney($comprasTotal))
        ->with('valorPago', $this->formatMoney($valorPago))
        ->with('valorAPagar', $this->formatMoney($valorAPagar));

    }

    public function newChanges(){
        return View::make('novidades');
    }

    public function formatMoney($input){
        return 'R$'.$input;
    }

}