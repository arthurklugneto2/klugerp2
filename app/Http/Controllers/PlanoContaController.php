<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\PlanoContaService;
use Validator;
use View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class PlanoContaController extends Controller
{
 
    protected $planoContaService;
    
	public function __construct(PlanoContaService $planoContaService)
	{
        $this->middleware('auth');
        $this->planoContaService = $planoContaService;
	}
	
	public function index()
	{
		$planos = $this->planoContaService->getAll();
	
		return View::make('planoContas.index')
		->with('planos', $planos);
	}
	
	public function create()
	{
		return View::make('planoContas.create');
	}
	
	public function store(Request $request)
	{
		$rules = array(
				'nome'  => 'required',
				'tipo'  => 'required',
		);
		$validator = Validator::make($request->input(), $rules);
	
		if ($validator->fails()) {
			return Redirect::to('planoContas/create')
			->withErrors($validator);
		} else {
	
			$this->planoContaService->save($request->input());
	
			Session::flash('message', 'Plano de Contas adicionada com sucesso!');
			return Redirect::to('planoContas');
		}
	}
	
	public function show($id)
	{
		$plano = $this->planoContaService->findById($id);
	
		return View::make('planoContas.show')
		->with('plano', $plano);
	}
	
	public function edit($id)
	{
		$plano = $this->planoContaService->findById($id);
	
		return View::make('planoContas.edit')
		->with('plano', $plano);
	}
	
	public function update(Request $request,$id)
	{
		$rules = array(
				'nome'  => 'required',
				'tipo'  => 'required',
		);
		$validator = Validator::make($request->input(), $rules);
	
		if ($validator->fails()) {
			return Redirect::to('planoContas/create')
			->withErrors($validator);
		} else {
	
			$this->planoContaService->update($request->input(),$id);
	
			Session::flash('message', 'Plano de Contas alterado com sucesso!');
			return Redirect::to('planoContas');
		}
	}
	
	public function destroy($id)
	{
		$this->planoContaService->remove($id);
	
		Session::flash('message', 'Plano de Contas apagada com sucesso!');
		return Redirect::to('planoContas');
	}	
	
}
