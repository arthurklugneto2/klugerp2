@extends('layouts.application') @section('content')
<div class="m-content">
    <div class="row">
        <div class="col-md-12">
            <div class="m-portlet m-portlet--metal m-portlet--head-solid-bg">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--font-transform-u" style="color:#302B3E;">
                                <i class="flaticon-gift"></i><span style="margin:24px;">Parâmetros de Sistema</span>
                            </span>
                            <h3 class="m-portlet__head-text m--font-primary ">
                            <a href="{{ URL::to('home') }}" class="m-portlet__nav-link btn btn-danger m-btn m-btn--air">
                                    Cancelar
                                </a>
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                <!-- VALIDATIONS ERRORS----------------------------- -->
                <div class="row">
                    <div class="col-md-12">{{ Html::ul($errors->all()) }}</div>
                </div>
                <!-- ----------------------------------------------- -->
                {{ Form::open(array('url' => '/parametros/update', 'method' => 'POST')) }}

					<div class="form-group">
						{{ Form::label('nomeEmpresa', 'Nome da Empresa') }} 
						{{ Form::text('nomeEmpresa', $parametro['nomeEmpresa'], array('class' =>'form-control')) }}
					</div>

                    <div class="form-group">
                        {{ Form::label('corCabecalho', 'Cor do Cabeçalho') }} 
                        <select name='corCabecalho' class="m_header_colorpicker">
                            <option class="m_header_colorpicker_item" value='#e83e8c' style='background-color:#e83e8c'>Rosa Escuro</option>
                            <option class="m_header_colorpicker_item" value='#f66d9b' style='background-color:#f66d9b'>Rosa</option>
                            <option class="m_header_colorpicker_item" value='#e3342f' style='background-color:#e3342f'>Vermelho</option>
                            <option class="m_header_colorpicker_item" value='#f6993f' style='background-color:#f6993f'>Laranja</option>
                            <option class="m_header_colorpicker_item" value='#38c172' style='background-color:#38c172'>Verde</option>
                            <option class="m_header_colorpicker_item" value='#3490dc' style='background-color:#3490dc'>Azul</option>  
                            <option class="m_header_colorpicker_item" value='#343a40' style='background-color:#343a40'>Dark</option>  
                            <option class="m_header_colorpicker_item" value='#4dc0b5' style='background-color:#4dc0b5'>Indigo</option>                            
                        </select>
					</div>

					{{ Form::submit('Gravar', array('class' => 'btn btn-primary')) }}
				
				{{ Form::close() }}                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection