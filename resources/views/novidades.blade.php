@extends('layouts.application') @section('content')
<div class="m-content">
    <div class="row">
        <div class="col-md-12">
            <h6>Versão 1.0.1</h6>
            <hr>
            <p><small>Ajustes iniciais <strong>Gugalu</strong></small></p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h6>Versão 1.0.0</h6>
            <hr>
            <p><small>Versão inicial adicionado ao repositório</small></p>
        </div>
    </div>
</div>

@endsection
