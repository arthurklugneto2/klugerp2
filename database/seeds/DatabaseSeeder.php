<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            FormaPagamentoTableSeeder::class,
            CategoriasProdutoTableSeeder::class,
            FornecedoresTableSeeder::class,
            ClientesTableSeeder::class,
            PlanoContasTableSeeder::class,
            PreferencesTableSeeder::class
        ]);
    }
}
