<?php

use Illuminate\Database\Seeder;

class CategoriasProdutoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $categorias = [
            ['id'=>'1','nome'=>'Categoria Padrão','created_at'=>Carbon\Carbon::now(),'updated_at'=>Carbon\Carbon::now()]
        ];

        DB::table('categorias_produtos')->insert($categorias);
        
    }
}
