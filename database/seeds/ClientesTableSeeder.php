<?php

use Illuminate\Database\Seeder;

class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clients = [
            ['id'=>'1','nome'=>'Cliente Padrão','created_at'=>Carbon\Carbon::now(),'updated_at'=>Carbon\Carbon::now()]
        ];

        DB::table('clientes')->insert($clients);
    }
}
