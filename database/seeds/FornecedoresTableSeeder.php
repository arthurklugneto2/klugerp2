<?php

use Illuminate\Database\Seeder;

class FornecedoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fornecedores = [
            ['id'=>'1','nome'=>'Fornecedor Padrão','tipo'=>'juridica','created_at'=>Carbon\Carbon::now(),'updated_at'=>Carbon\Carbon::now()]
        ];

        DB::table('fornecedors')->insert($fornecedores);
        
    }
}
