<?php

use Illuminate\Database\Seeder;

class PreferencesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $preference = [
            ['id'=>'1','nomeEmpresa'=>'Nome da Empresa','created_at'=>Carbon\Carbon::now(),'updated_at'=>Carbon\Carbon::now()]
        ];

        DB::table('Preferences')->insert($preference);
    }
}
